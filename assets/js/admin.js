// declare a new module called 'myApp', and make it require the `ng-admin` module as a dependency
var myApp = angular.module('myApp', ['ng-admin']);

// declare a function to run when the module bootstraps (during the 'config' phase)
myApp.config(['NgAdminConfigurationProvider', function (nga) {

	// create an admin application
	var admin = nga
		.application('CCR Admin')
		.baseApiUrl('http://localhost:1337/');

	var user = nga.entity('user');
	// set the fields of the user entity list view
	user.listView().fields([
		nga.field('id'),
		nga.field('login'),
		nga.field('role'),
		nga.field('createdAt'),
		nga.field('updatedAt')
	]).listActions(['delete']);

	user.creationView().fields([
		nga.field('login'),
		nga.field('role'),
		nga.field('password'),
	]);

	// add the user entity to the admin application
	admin.addEntity(user);

	var channels = nga.entity('channel');
	channels.listView().fields([
		nga.field('id'),
		nga.field('title'),
		nga.field('location'),
		nga.field('language'),
		nga.field('lastBroadcastAt'),
		nga.field('createdAt'),
		nga.field('updatedAt')
	]);
	admin.addEntity(channels);
	// attach the admin application to the DOM and execute it
	nga.configure(admin);
}]);
