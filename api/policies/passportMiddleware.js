var passport = require('passport');
var http = require('http');

module.exports = function (req, res, next) {
	// Initialize Passport
	passport.initialize()(req, res, function () {
		// Use the built-in sessions
		passport.session()(req, res, function () {
			res.locals.user = req.user;

			var methods = ['login', 'logIn', 'logout', 'logOut', 'isAuthenticated', 'isUnauthenticated'];
			if (req.isSocket) {
				for (var i = 0; i < methods.length; i++) {
					req[methods[i]] = http.IncomingMessage.prototype[methods[i]].bind(req);
				}
			}

			next();
		});
	});
};
