module.exports = function(req, res, next) {
	if (!req.isAuthenticated()) {
		return res.json(401);
	}

	if (req.user.role != 'admin') {
		return res.json(401);
	}

	return next();
};

