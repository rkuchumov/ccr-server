
module.exports = function channelExists(req, res, next) {
	var channel = req.param("channel");

	if (!channel)
		return res.badRequest("Empty channel id");

	Channel.findOne({
		id: channel
	}).exec(function(err, record) {
		if (err)
			return next(err);

		if (!record)
			return res.badRequest("Channel is not found");

		return next();
	});
};
