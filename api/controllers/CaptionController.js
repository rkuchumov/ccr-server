/**
 * CaptionController
 *
 * @description :: Server-side logic for managing Captions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var _ = require('underscore');

module.exports = {
	show: function (req, res) {
		// TODO: implement
		return res.send('not implemented');
	},

	index: function(req, res) {
		var which = {
			channel:  req.param('channel')
		};
		// TODO: more criterias

		if (req.isSocket) {
			var room = '/channels/' + which.channel + '/captions';

			sails.sockets.join(req, room, function(err) {
				if (err)
					res.serverError(err);
			});
		}

		Caption.find(which).exec(function(err, captions) {
			if (err)
				return res.badRequest(err);

			if (req.isSocket)
				Caption.subscribe(req, _.pluck(captions, 'start'));

			res.json(captions);
		});
	},

	create: function(req, res) {
		var caption = _.pick(req.allParams(),
			'channel', 'start', 'duration', 'mode', 'text');
		// console.log(caption);

		Caption.create(caption).exec(function(err, created) {
			if (err)
				return res.badRequest(err);

			var room = '/channels/' + caption.channel + '/captions';
			sails.sockets.broadcast(room, room + '/create', created);

			return res.ok();
		});
	},

	update: function(req, res) {
		var which = {
			start:    req.param('start'),
			channel:  req.param('channel')
		};

		var caption = _.pick(req.allParams(),
			'channel', 'start', 'duration', 'mode', 'text');

		console.log(caption);

		Caption.update(which, caption).exec(function(err, updated) {
			if (err) {
				return res.badRequest(err);
			}

			if (updated.length == 0) {
				console.log(updated, err);
				return res.ok();
			}

			console.assert(updated.length == 1, // TODO handle th case when == 0
				"Only one caption is supposed to be updated");

			var room = '/channels/' + caption.channel + '/captions';
			sails.sockets.broadcast(room, room + '/update', updated[0]);

			return res.ok();
		});
	},

};

