/**
 * ChannelController
 *
 * @description :: Server-side logic for managing Channels
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var _ = require('underscore');

module.exports = {
	index: function (req, res) {
		if (req.isSocket)
			Channel.watch(req);

		Channel.find({}).exec(function(err, channels) {
			if (err)
				return res.badRequest(err);

			if (req.isSocket)
				Channel.subscribe(req, _.pluck(channels, 'id'));

			// TODO: hide updatedAt, createdAt
			res.json(channels);
		});
	},

	show: function (req, res) {
		// TODO: implement
		return res.send('not implemented');
	},

	create: function (req, res) {
		var channel = _.pick(req.allParams(),
			'id', 'title', 'location', 'language', 'descripion');

		Channel.create(channel).exec(function(err, created) {
			if (err)
				return res.badRequest(err);

			Channel.publishCreate(created);

			return res.ok();
		});
	},

	update: function (req, res) {
		var which = {
			id: req.param('channel')
		};

		var channel = _.pick(req.allParams(),
			'id', 'title', 'location', 'language', 'descripion');

		Channel.update(which, channel, function(err, updated) {
			if (err)
				return res.badRequest(err);

			Channel.publishUpdate(which.id, updated);

			return res.ok();
		});
	},
};

