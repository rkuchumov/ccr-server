
module.exports = {
	index: function (req, res) {

		User.find({}).exec(function(err, users) {
			if (err)
				return res.badRequest(err);

			res.json(users);
		});
	},

	create: function (req, res) {
		var user = _.pick(req.allParams(), 'login', 'password', 'role');

		console.log('Creating user: ', user);

		User.create(user).exec(function(err, created) {
			if (err) {
				return res.badRequest(err);
			}

			User.publishCreate(created);

			return res.ok();
		});
	},
};
