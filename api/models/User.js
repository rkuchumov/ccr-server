/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var bcrypt = require('bcrypt');

module.exports = {
	connection: 'redis',

	attributes: {
		login: {
			type: 'string',
			required: true,
			unique: true,
		},

		password: {
			type: 'string',
			required: true,
		},

		role: {
			type: 'string',
			defaultsTo: 'user',
		},

		toJSON: function() {
			var obj = this.toObject();
			delete obj.password;
			return obj;
		},
	},

	beforeCreate: function(user, cb) {
		bcrypt.genSalt(10, function(err, salt) {
			bcrypt.hash(user.password, salt, function(err, hash) {
				if (err) {
					console.log(err); // TODO: use sails
					cb(err);
				} else {
					user.password = hash;
					cb();
				}
			});
		});
	}
};

