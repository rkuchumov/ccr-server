/**
 * Caption.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	connection: 'redis',

	attributes: {
		start: {
			type: 'integer',
			primaryKey: true,
			required: true
		},

		channel: {
			model: 'channel',
			required: true
		},

		duration: {
			type: 'integer',
			required: true
		},

		text: {
			type: 'string',
			maxLength: 300,
			required: true,
		},

		mode: {
			type: 'string',
			maxLength: 3,
			required: true
		}


	}
};

