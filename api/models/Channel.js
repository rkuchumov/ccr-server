/**
 * Channel.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	connection: 'redis',

	attributes: {
		id: {
			type: 'string',
			primaryKey: true,
			required: true
		},

		lastBroadcastAt: {
			type: 'datetime',
			defaultsTo: function() {
				return new Date();
			}
		},

		title: {
			type: 'string',
			required: true
		},

		location: {
			type: 'string'
		},

		language: {
			type: 'string'
		},

		descripion: {
			type: 'text'
		},

		captions: {
			collections: 'caption',
			via: 'channel'
		},
	},

};

